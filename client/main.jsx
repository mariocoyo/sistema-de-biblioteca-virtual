import React,{Component} from 'react';
import { Meteor } from 'meteor/meteor';
import {Tracker} from 'meteor/tracker'
import { render } from 'react-dom';
import {routes,checkAuthUser} from '/imports/routes/Routes'//array de rutas de navegacion
import {BrowserRouter,Redirect,Route,Switch} from 'react-router-dom'
import Principal from '/imports/view/principal/Principal'
import SwitchRoutes from '/imports/routes/SwitchRoutes';//creaddo para renderizar nuestras subrutas
//moment =require('moment')
T9n =require ('meteor-accounts-t9n').T9n

Tracker.autorun(function(){
  T9n.map('es', require('meteor-accounts-t9n/build/es'))
  T9n.setLanguage('es')
  const authenticated = !!Meteor.userId()
  console.log(`el id de usuario es ${authenticated}`)
  checkAuthUser(authenticated)

  if(!authenticated){
    Meteor.logout(function(err){
      console.log('Cerrando secion')
  });
}
})

class App extends Component{
  render(){
    return (
      <BrowserRouter>
           <Switch>
              {/*<Route exact path="/" component={Principal} />*/}
              <Route exact path="/">
                <Redirect to="/bienvenido/principal"/>
              </Route>
              <Route exact path="/bienvenido">
                <Redirect to="/bienvenido/principal"/>
              </Route>
              {
                  routes.map((route,i)=>{
                    
                    return  <SwitchRoutes key={i} {...route}/>
                  })
              } 
           </Switch>
      </BrowserRouter>
    )
  }
}

Meteor.startup(() => {
  render(<App/>, document.getElementById('react-target'));
});

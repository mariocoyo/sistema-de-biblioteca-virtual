import React, {Component} from 'react'
import Signin from  '/imports/view/auth/Signin'
import Layout from '/imports/view/layout/Layout'
import Principal from '/imports/view/principal/Principal'
import Main from '/imports/view/principal/Main'
import { useLocation } from 'react-router'
import Home from '/imports/view/home/Home'
import Signup from '/imports/view/auth/Signup'
import CreatePublications from '/imports/view/publications/CreatePublications'
import userPublications from '../view/publications/userPublications'
import { createBrowserHistory } from "history";
import CategoryRoutes from './CategoryRoutes'
import PublicationsRoutes from './PublicationsRoutes'


const history = createBrowserHistory();

function NoMatch(){
    let location=useLocation()
    return(
        <div>
            <h3>
                La pajina solicitada no existe <code>{location.pathname}</code>
            </h3>
        </div>
    )
}
export const routes =[ //array de objetos donde estan mis rutas con mis componentes que quiero que renderice
    {
        path:'/bienvenido',
        component:Principal,
        authenticated:false,
        routes:[
            {
                path:'/bienvenido/principal',
                component:Main
            },
            {
                path:'/bienvenido/login',
                component:Signin
            },
            {
                path:'/bienvenido/registro',
                component:Signup
            },
        ]    
    },
    
    {
        path:'/dasboard',//rutas
        component:Layout,//componentes 
        authenticated:true,
        routes:[
            {
                path: "/dasboard/home",
                component:Home
            },
            {
                path: "/dasboard/create-publications",
                component:CreatePublications
            },
            {
                path: "/dasboard/users-publications",
                component:userPublications
            },
            ...PublicationsRoutes,
            ...CategoryRoutes
        ]
    },
    {
        path:'*',
        component:NoMatch
    }
]

function getAllRoutesAuthenticated(routes){
    let routesAuthenticate=[]
    routes.forEach((value,index)=>{
        if(JSON.stringify(value.authenticated)===JSON.stringify(true)){
            routesAuthenticate.push(value,index)
            if(value.routes){
            value.routes.forEach((v,i) =>{
                routesAuthenticate.push(v.path)
            })
        }
        }
    })
    return routesAuthenticate
}

export const checkAuthUser=function(authenticated){
    //console.log('el usuario esta autenticado:'+authenticated)
    const path =history.location.pathname
    const isauthenticatedPage =getAllRoutesAuthenticated(routes).includes(path)
    if(authenticated && isauthenticatedPage){
       // console.log('el usuario puede ingresar a las pajinas que necesitan autenticacion')
        history.replace('/dasboard/home')
    }
    else if(!authenticated && isauthenticatedPage){
          // console.log('el usuario no esta logueado')
           history.replace('/')
           location.reload()
    }
}
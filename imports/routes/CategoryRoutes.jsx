import CreateCategory from '../view/category/CreateCategory'
import allCategory from '../view/category/allCategory'

export default routes =[ //array de objetos donde estan mis rutas con mis componentes que quiero que renderice
    {
        path:'/dasboard/create-category',
        component:CreateCategory,
        permission:"createCategory"
           
    },
    {
        path:'/dasboard/all-category',
        component:allCategory,
        permission:"viewallCategory"
    }
]
import CreatePublications from '/imports/view/publications/CreatePublications'
import userPublications from '../view/publications/userPublications'
import CreateCategory from '../view/category/CreateCategory'
import EditarPublicacion from '../view/publications/EditarPublicacion'
export default routes =[ //array de objetos donde estan mis rutas con mis componentes que quiero que renderice
        {
            path: "/dasboard/create-publications",
            component:CreatePublications,
            permission:"createpublication"
        },
        {
            path: "/dasboard/edit-publications",
            component:EditarPublicacion,
            permission:"editpublication"
        },
        {
            path: "/dasboard/users-publications",
            component:userPublications,
            permission:"allpublicationuser"
        },
]
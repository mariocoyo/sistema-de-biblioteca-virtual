import React, { Component } from 'react'

export default class FileUpload extends Component {
    constructor(props){
        super(props)
        this.state={
            uploading:[],
            progress:0,
            inProgress:false
        }
    }
    uploadIt=(e)=>{
        e.preventDefault()
        let selt=this
        this.props.changeFileInput({file:e.currentTarget.files,self:selt})
    }
    showUploads(){

    }
    render() {
        return (
            <div>
                <input type="file" id="fileinput" ref="fileinput" onChange={this.uploadIt}/>        
            </div>
        )
    }
}

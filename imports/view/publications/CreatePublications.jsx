import React, { Component } from 'react'
import {publicationsClass} from '../../models/publications/class'//importamos la clase de nuestra coleccion o tablas
import {withTracker} from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor'
import DatePicker from "react-datepicker";
import {categoryClass} from '../../models/category/class'
import FileUpload from '../../components/FileUpload';
import uploadFiles from '../../utils/upload'

class CreatePublications extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                title:null,
                description:null,
                phone:null,
                price:null,
                startDate:new Date(),
                endDate:new Date(),
                image:null,
                category:null
            },
            
            errors:{}
        }
    }
    validateForm=()=>{
        const {form}=this.state//obtener la propiedad form fr this.state
        let errorsform ={}//objeto vacio
        let formsIsValid=true//verificar si el formulario es valido o no
        if(!form.title){
            formsIsValid=false
            errorsform.title="El titulo no puede estar vacio"
        }
        if(!form.description){
            formsIsValid=false
            errorsform.description="La descripcion no puede estar vacio"
        }
        if(!form.phone){
            formsIsValid=false
            errorsform.phone="El telefono no puede estar vacio"
        }
        if(!form.price){
            formsIsValid=false
            errorsform.price="El precio no puede estar vacio"
        }
        if(!form.startDate){
            formsIsValid=false
            errorsform.startDate="La fecha inicial no puede estar vacio"
        }
        if(!form.endDate){
            formsIsValid=false
            errorsform.endDate="La fecha final no puede estar vacio"
        }
        this.setState({errors:errorsform})//cambia el valor de la propiedad errors en this.state con los nuevos valores del metodo que estan en la variable errorsform
        return formsIsValid
    }
    //metodo para enviar el formulario(cuando se preciona el boton de enviar)
    //createNewPublications(e){//no va tener acceso a lo que las variables dentro del componente
    createNewPublications=(e)=>{//si va tener acceso a las variables dentro del componente
       e.preventDefault()
        if(this.validateForm()){
           //alert('enviando formulario')
           //console.log(this.state.form)
           const newpublicacion= new publicationsClass()
           const {form}=this.state//obtener la propiedad form de this.state
           form.price=parseInt(form.price)
           console.log(form)
           const {form:{image}}=this.state
           const uf=new uploadFiles(image.file,image.self)
           uf.newUpload(function(error,success){
               if(error){
                   console.log('******************')
                   console.log(error)
                   console.log('******************')
               }else{
                form.image=success._id
                newpublicacion.callMethod('newPublication',form,(error,result)=>{//newPublications esta declarado en la extecion de la clase publicationsClass en el revidor
                    if(error){
                        console.log('error!!!!')
                        console.log(error)
                        alert(error)
                    }
                    else{
                        console.log('!!!!')
                        alert(result)
                        console.log(result)
                        document.getElementById('newPublication').reset()
                    }
                })
               }
           })
           
      }
       else{
           alert('el formulario tiene errores')
       }
    }
    changeTextImput =(e)=>{//(e) esta llegando todas las propiedades que pueda tener el valor <input>
        const value=e.target.value//recupera el valor que se esta escribiendo en el imput
        const property=e.target.name//recupera la propiedad name del imput
        //precState=> esta recuperndo el valor anterio de this.state
        //({form:{...preventState.form,title:value}})=> retorna el cambio hecho en propiedad form
        //de this.state lo esta de la siguinete manera:
        // ""...preventState.form" esto esta sacando todas las propiedades de "form" (title:null, descripcion:null, phone:null,price:null,startDate:null, endDate:null)
        //los siguintes valores depues de la coma(,) si la propiedad ya existe en (...preventState.form) remplazara el valor que tenia pero si no existe
        //creara una propiedad con un nuevo valor
        this.setState(prevState=>(
            {form:{
                ...prevState.form,
                [property]:value,//[property] la variable property lo ponemos dentro de corchetes para que sepa el metodo que esa propieda va ser dinamica puede tener valores(title,description, price, etc) de form
            }
        }
        ))
    }
    changeDateInput=(type,date)=>{
       this.setState(prevState=>(
           {form:{
               ...prevState.form,
               [type]:date
           }}
       ))
    }
    changeSelectInput=(e)=>{
        const value =e.target.value
        this.setState(prevState=>(
            {form:{
                   ...prevState.form,
                   category:value
               }
            }
        ))
    }
    changeFileInput=(file)=>{
        console.log(file)
        const inputFile=file.file
        //const input=e.target
        //console.log(value)
        if(inputFile && inputFile[0]){
            let reader=new FileReader()
            reader.onload=function(v){
                $('#previewimage').attr('src',v.target.result)
            }
            reader.readAsDataURL(inputFile[0])
            this.setState(prevState=>(
                {form:{
                       ...prevState.form,
                       image:file
                   }
                }
            ))
        }
    }
    render() {
        const {errors}=this.state//recuperar la propiedad errors de this.state
        const {categorys,subscriptionCategory}=this.props
        return (
            <div>
                    <section className="section">
                        <div className="section-body">
                            <div className="row">
                                <div className="col-12 col-md-6 col-lg-12">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4>Crear Nueva Publicaion</h4>
                                        </div>
                                        {!subscriptionCategory.ready()?
                                            <h1>Cargando</h1>
                                        :
                                        <div className="card-body">
                                        <form onSubmit={this.createNewPublications} id="newPublication">
                                            <div className="row">
                                                <div className="col-6">
                                                <div className="form-group">
                                            <label>Titulo de publicacion</label>
                                            <input type="text" className={errors.title?"form-control is-invalid":"form-control"} name={'title'} onChange={this.changeTextImput}autoComplete="off"/>
                                            {
                                                errors.title?
                                                <div className="invalid-feedback">
                                                    {errors.title}
                                                </div>
                                              :
                                                null
                                            }
                                        </div>
                                        <div className="form-group">
                                            <label>Descripcion</label>
                                            <input type="text" className={errors.description?"form-control is-invalid":"form-control invoice-input"} name={'description'} onChange={this.changeTextImput}autoComplete="off"/>
                                            {
                                                errors.description?
                                                <div className="invalid-feedback">
                                                    {errors.description}
                                                </div>
                                              :
                                                null
                                            }
                                        </div>
                                        <div className="form-group">
                                            <label>Numero de Telefono</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <div className="input-group-text">
                                                        <i className="fas fa-phone"></i>
                                                    </div>
                                                </div>
                                                <input type="text" className={errors.phone?"form-control phone-number is-invalid":"form-control phone-number"} name={'phone'} onChange={this.changeTextImput}autoComplete="off"/>
                                                {
                                                errors.phone?
                                                <div className="invalid-feedback">
                                                    {errors.phone}
                                                </div>
                                                :
                                                    null
                                                }
                                            </div>
                                        </div>
                                      
                                        <div className="form-group">
                                            <label>Precio</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <div className="input-group-text">
                                                        $
                                                    </div>
                                                </div>
                                                <input type="text" className={errors.price?"form-control currency is-invalid":"form-control currency"} name={'price'} onChange={this.changeTextImput}autoComplete="off"/>
                                                {
                                                errors.price?
                                                <div className="invalid-feedback">
                                                    {errors.price}
                                                </div>
                                                :
                                                    null
                                                }
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label>Seleccioname una categoria</label>
                                            <select className="form-control" onChange={this.changeSelectInput}>
                                                <option defaultValue disabled="disabled">Seleccione una categoria</option>
                                                {
                                                    categorys.map((category,key)=>{
                                                        return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                                    })
                                                }
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label>Archivo</label>
                                            {/*<input type="file" className="form-control" name={'file'} onChange={this.changeFileInput}/>*/}
                                            <FileUpload changeFileInput={this.changeFileInput}/>
                                            
                                        </div>
                                        <div className="form-group">
                                            <label>Fecha de Publicacion</label>
                                            {//<input type="text" className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"}
                                                //placeholder="YYYY/MM/DD" name={'startDate'} onChange={this.changeTextImput}/>
                                            }
                                                <DatePicker selected={this.state.form.startDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} name={'endDate'}
                                                onChange={date =>{ 
                                                    this.changeDateInput('startDate',date)
                                                }} />
                                            {
                                                errors.startDate?
                                                <div className="invalid-feedback" style={{display:'block'}}>
                                                    {errors.startDate}
                                                </div>
                                                :
                                                    null
                                            }
                                        </div>
                                        <div className="form-group">
                                            <label>Fecha de Culminacion</label>
                                            {//<input type="text" className={errors.endDate?"form-control datemask is-invalid":"form-control datemask"}
                                               // placeholder="YYYY/MM/DD" name={'endDate'} onChange={this.changeTextImput}/>
                                            }
                                            <DatePicker selected={this.state.form.endDate} className={errors.endDate?"form-control datemask is-invalid":"form-control datemask"} name={'endDate'}
                                                onChange={date =>{ 
                                                    this.changeDateInput('endDate',date)
                                                }} 
                                                />
                                            {
                                                errors.endDate?
                                                <div className="invalid-feedback" style={{display:'block'}}>
                                                    {errors.endDate}
                                                </div>
                                                :
                                                    null
                                                }
                                        </div>
                                          
                                                </div>
                                                <div className="col-md-6 d-flex justify-content-center">
                                                    <div className="author-box-center">
                                                        <img alt="image" src="/dashboard/img/users/Images-icon.png" id="previewimage" style={{width:'100%', height:'500'}}
                                                            className="rounded-circle author-box-picture" />
                                                        <div className="clearfix" />
                                                        <div className="author-box-name">
                                                        </div>
                                                        <div className="author-box-job d-flex justify-content-center">Lista Previa</div>
                                                    </div>

                                                </div>
                                            </div>
                                       <button type="submit" className="btn btn-icon icon-left btn-primary"><i className="far fa-edit">Primary</i></button>
                                        </form>
                                    </div>
                                        }
                                        
                                    </div>
                               </div>
                          </div>
                        </div>
                    </section>
            </div>
        )
    }
}
export default withTracker((props)=>{
    const subscriptionCategory=Meteor.subscribe('category',{},'getCategory')
    const categorys=categoryClass.find().fetch()
    console.log(categorys)
    return {categorys,subscriptionCategory}
})(CreatePublications)
import React, { Component } from 'react'
import {withTracker} from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor'
import { publicationsClass } from '../../models/publications/class'


class userPublications extends Component {
    changestatepub=(pub)=>{
        console.log(pub)
        pub.callMethod('updateStatePublication',(error,result)=>{//newPublications esta declarado en la extecion de la clase publicationsClass en el revidor
            if(error){
                alert(error)
            }
            else{
                alert('cambio de estado exitoso')
            }
        })
    }
    render() {
        const {publications,susbcriptionPublications, history}=this.props//devuelve los campos  publications susbcriptionPublications de this.props
        return (
           <div>
               <div className="row">
                   <div className="col-12 col-md-12 col-lg-12">
                       <div className="card">
                           <div className="card-header">
                               <h4>Todos mis libros</h4>
                           </div>
                           <div className="card-body">
                               <div className="table-responsive">
                                   <table className="table table-bordered table-md">
                                       <thead>
                                           <tr>
                                               <th>#</th>
                                               <th>Titulo</th>
                                               <th>fecha de creacion</th>
                                               <th>Estado</th>
                                               <th>Acciones</th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                       {
                                            publications.map((data,key)=>{
                                            return <tr key={`pub_${key}`}>
                                                <td>{key+1}</td>
                                                <td>{data.title}</td>
                                                <td>{moment(data.created_view).locale('es').format('dddd DD [de] MMMM [de] YYYY')}</td>
                                            
                                                <td>
                                                    {data.active?<div className="badge badge-success">Active</div>:<div className="badge badge-danger">No activo</div>}
                                                </td>
                                                <td>
                                                <button className="btn btn-primary" onClick={()=>{
                                                    history.push({
                                                        pathname: '/dasboard/edit-publications',
                                                        state:{publication:data._id}
                                                    })
                                                }}>Editar publicacion</button>
                                                <button className="btn btn-danger ml-3" onClick={()=>{this.changestatepub(data)}}>Dar de baja publicacion</button>
                                                </td>
                                            </tr>
                                            })
                                        }
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                           <div className="card-footer text-right">
                               <nav className="d-inline-block">
                                   <ul className="pagination mb-0">
                                       <li className="page-item disabled">
                                           <a className="page-link" href="#" tabIndex={-1}><i
                                                   className="fas fa-chevron-left" /></a>
                                       </li>
                                       <li className="page-item active"><a className="page-link" href="#">1 <span
                                                   className="sr-only">(current)</span></a></li>
                                       <li className="page-item">
                                           <a className="page-link" href="#">2</a>
                                       </li>
                                       <li className="page-item"><a className="page-link" href="#">3</a></li>
                                       <li className="page-item">
                                           <a className="page-link" href="#"><i className="fas fa-chevron-right" /></a>
                                       </li>
                                   </ul>
                               </nav>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

            )
    }
}

export default withTracker((props)=>{
    const susbcriptionPublications=Meteor.subscribe('publications',{},'getPublications' )
    //console.log(susbcriptionPublications)
    const publications=publicationsClass.find().fetch()
    //console.log(publications)
    return {publications,susbcriptionPublications}
})(userPublications)

import React, { Component } from 'react'
import {Link,Switch} from 'react-router-dom'

import Nav from '/imports/components/Nav'
import SwitchRoutes from '/imports/routes/SwitchRoutes'

  

export default class Principal extends Component{
    constructor(props){
        super(props)
        this.state={
            loader:true
        }
    }
    componentDidMount(){
        import '/imports/assets/principal/css'
        import '/imports/assets/principal/js'
        import desingPrincipal from '/imports/assets/principal/js/main'
        const mthis=this
        setTimeout(function(){//setTimeout voya ejecutar funcion pasado el tiempo pasado que me des como parametro
            mthis.setState({loader:false})
            desingPrincipal()
            $('#preloader-active').fadeOut('slow');
        },2000)
        
    }
    render(){
        const {routes}=this.props
        return (
            <div>
                {this.state.loader?
                <div id="preloader-active">
                    <div className="preloader d-flex align-items-center justify-content-center">
                        <div className="preloader-inner position-relative">
                            <div className="preloader-circle"></div>
                            <div className="preloader-img pere-text">
                                <img src="/principal/img/loder.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                :
                <div>
                    <header>

                        <div className="header-area header-transparent">
                            <div className="main-header ">
                                <div className="header-bottom  header-sticky">
                                    <div className="container-fluid">
                                        <div className="row align-items-center">

                                            <div className="col-xl-2 col-lg-2">
                                                <div className="logo">
                                                    <a href="/"><img src="principal/img/logo.png" alt="" /></a>
                                                </div>
                                            </div>
                                            <div className="col-xl-10 col-lg-10">
                                                <div
                                                    className="menu-wrapper d-flex align-items-center justify-content-end">

                                                    <div className="main-menu d-none d-lg-block">
                                                        <nav>
                                                            <ul id="navigation">
                                                                <li><a href="/">Principal</a></li>

                                                                <li className="button-header margin-left "><a href="/"
                                                                        className="btn">Sign Up</a></li>
                                                                <li className="button-header">
                                                                    <Link to="/bienvenido/registro" href="login.html"
                                                                        className="btn3">Registrarme
                                                                    </Link>
                                                                </li>
                                                                <li className="button-header">
                                                                    <Link to="/bienvenido/login" href="login.html"
                                                                        className="btn3">Iniciar sesion
                                                                    </Link>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-12">
                                                <div className="mobile_menu d-block d-lg-none"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </header>
                    <main>
                        <Switch>
                            {
                            routes.map((route,i)=>{
                                return <SwitchRoutes key={i} {...route} />
                            })
                            }
                        </Switch>

                    </main>
                    <footer>
                        <div className="footer-wrappr " data-background="/principal/img/footer-bg.png">
                            <div className="footer-area footer-padding ">
                                <div className="container">
                                    <div className="row d-flex justify-content-between">
                                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                            <div className="single-footer-caption mb-50">
                                                <div className="footer-logo mb-25">
                                                   
                                                </div>
                                                <div className="footer-tittle mb-50">
                                                    <p>Subscribe our newsletter to get updates about our services</p>
                                                </div>
                                                <div className="footer-form">
                                                    <div id="mc_embed_signup">
                                                        <form target="_blank"
                                                            action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                                            method="get" className="subscribe_form relative mail_part">
                                                            <input type="email" name="EMAIL" id="newsletter-form-email"
                                                                />
                                                            <div className="form-icon">
                                                                <button type="submit" name="submit"
                                                                    id="newsletter-submit"
                                                                    className="email_icon newsletter-submit button-contactForm">
                                                                    Subscribe
                                                                </button>
                                                            </div>
                                                            <div className="mt-10 info"></div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div className="footer-social mt-50">
                                                    <a href="#"><i className="fab fa-twitter"></i></a>
                                                    <a href="https://bit.ly/sai4ull"><i
                                                            className="fab fa-facebook-f"></i></a>
                                                    <a href="#"><i className="fab fa-pinterest-p"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-1 col-lg-1 col-md-1 col-sm-1"></div>
                                        <div className="col-xl-2 col-lg-2 col-md-4 col-sm-5">
                                            <div className="single-footer-caption mb-50">
                                                <div className="footer-tittle">
                                                    <h4>Company</h4>
                                                    <ul>
                                                        <li><a href="#">Why choose us</a></li>
                                                        <li><a href="#"> Review</a></li>
                                                        <li><a href="#">Customers</a></li>
                                                        <li><a href="#">Blog</a></li>
                                                        <li><a href="#">Carrier</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-2 col-lg-2 col-md-4 col-sm-5">
                                            <div className="single-footer-caption mb-50">
                                                <div className="footer-tittle">
                                                    <h4>Products</h4>
                                                    <ul>
                                                        <li><a href="#">Why choose us</a></li>
                                                        <li><a href="#"> Review</a></li>
                                                        <li><a href="#">Customers</a></li>
                                                        <li><a href="#">Blog</a></li>
                                                        <li><a href="#">Carrier</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-2 col-lg-2 col-md-4 col-sm-5">
                                            <div className="single-footer-caption mb-50">
                                                <div className="footer-tittle">
                                                    <h4>Support</h4>
                                                    <ul>
                                                        <li><a href="#">Technology</a></li>
                                                        <li><a href="#"> Products</a></li>
                                                        <li><a href="#">Customers</a></li>
                                                        <li><a href="#">Quality</a></li>
                                                        <li><a href="#">Sales geography</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="footer-bottom-area">
                                <div className="container">
                                    <div className="footer-border">
                                        <div className="row">
                                            <div className="col-xl-12">
                                                <div className="footer-copy-right text-center">
                                                    <p>
                                                        All rights reserved | This template is made with <i
                                                            className="fa fa-heart" aria-hidden="true"></i> by <a
                                                            href="https://colorlib.com" target="_blank">Colorlib</a>
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <div id="back-top">
                        <a title="Go to Top" href="#"> <i className="fas fa-level-up-alt"></i></a>
                    </div>
                </div>
                }

            </div>
        )
    }
}
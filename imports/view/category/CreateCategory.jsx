import React, { Component } from 'react'
import { categoryClass } from '../../models/category/class'
export default class createCategory extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{  
                name:null,
                description:null,
            },
            
            errors:{}
        }
    }
    validateForm=()=>{
        const {form}=this.state//obtener la propiedad form fr this.state
        let errorsform ={}//objeto vacio
        let formsIsValid=true//verificar si el formulario es valido o no
        if(!form.name){
            formsIsValid=false
            errorsform.name="El nombre de categoria no puede estar vacio"
        }
        const validateItIs= /^[a-zA-Z0-9\s\u00f1\u00d1]{6,}$/g
        if(!validateItIs.test(form.name)){
            formsIsValid=false
            errorsform['name']="El nombre de categoria no puede tener signos especiales y tiene que ser mayor o igual 6"
        }
        if(!form.description){
            formsIsValid=false
            errorsform.description="La descripcion no puede estar vacio"
        }
        this.setState({errors:errorsform})//cambia el valor de la propiedad errors en this.state con los nuevos valores del metodo que estan en la variable errorsform
        return formsIsValid
    }
    changeTextImput =(e)=>{//(e) esta llegando todas las propiedades que pueda tener el valor <input>
        const value=e.target.value//recupera el valor que se esta escribiendo en el imput
        const property=e.target.name//recupera la propiedad name del imput
        this.setState(prevState=>(
            {form:{
                ...prevState.form,
                [property]:value,//[property] la variable property lo ponemos dentro de corchetes para que sepa el metodo que esa propieda va ser dinamica puede tener valores(title,description, price, etc) de form
            }
        }
        ))
    }
    createNewPublications=(e)=>{
        e.preventDefault()
        const cc=new categoryClass
        const {form}=this.state
        if(this.validateForm()){
            const mthis=this
             //esto nos sirvio solo para crear roles y asignar al usuario  admin(no tenemos modulode administrador usuario para hacerlo dinamoico)
             /*Meteor.call('createRoles',form,function(error,resp){
                if(error){
                    console.log(error)
                    alert(error.reason)
                }
                else{
                    alert(resp)
                    console.log(resp)
                }
            })*/
            cc.callMethod('createNewCategory',form,(error, result)=>{
                if(error){
                    alert(error)
                }
                else{
                    alert(result)
                    document.getElementById('newCategory').reset()
                    mthis.setState({form:{
                        name:null,
                        description:null
                    }})
                }
            })
        }
    }
    render() {
        const {errors}=this.state
        return (
            <div>
                <div className="card">
                    <form onSubmit={this.createNewPublications} id="newCategory">
                        <div className="card-header">
                            <h4>Crear categoria</h4>
                        </div>
                        <div className="card-body">
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="name">Nombre de categoria</label>
                                    <input type="text" className={errors.name?'form-control is-invalid':'form-control'} id="name" name={'name'} placeholder="categoria"
                                        onChange={this.changeTextImput} autoComplete="off"/>
                                        {
                                                errors.name?
                                                <div className="invalid-feedback">
                                                    {errors.name}
                                                </div>
                                                :
                                                    null
                                        }
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="inputPassword4">Descripcion</label>
                                    <input type="text" className={errors.description?'form-control is-invalid':'form-control'} id="description" name={'description'}
                                        placeholder="description" onChange={this.changeTextImput} autoComplete="off"/>
                                          {
                                                errors.description?
                                                <div className="invalid-feedback">
                                                   {errors.description}
                                                </div>
                                             :
                                                  null
                                         }
                                </div>
                            </div>
                        </div>
                        <div className="card-footer">
                            <button type="submit" className="btn btn-primary">Crear categoria</button>
                        </div>
                    </form>
                </div>


            </div>
        )
    }
}

import React, { Component } from 'react'
import {Switch} from 'react-router-dom'
import { Meteor } from 'meteor/meteor';
import {Tracker} from 'meteor/tracker'
import {withTracker} from 'meteor/react-meteor-data'
import Nav from '/imports/components/Nav'
import Sidebar from '/imports/components/Sidebar'
import Home from '/imports/view/home/Home'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import runGraphics from '/imports/assets/dashboard/js/page'
import {Roles} from 'meteor/alanning:roles'
const feather=require('feather-icons')
import "react-datepicker/dist/react-datepicker.css";

class Layout extends Component{
    constructor(props){
        super(props)
        this.state={
            username:null,
            loader:true
        }
    }
    componentDidMount(){
        import '/imports/assets/dashboard/js'
        import '/imports/assets/dashboard/css'
        import dashboardDesing from '/imports/assets/dashboard/js/scripts'
        
        dashboardDesing()
    }
    componentDidUpdate(prevProps){//revisa si han existido cambios en la variables props y state
        if(this.props.username!==prevProps.username){
            console.log(this.props.username)
            runGraphics()
            feather.replace();
            setTimeout(function(){
                $(".loader").fadeOut("slow");
            },2000)
            
        }
    }
    render(){
        const {routes,username} =this.props//recupera tu propiedad routes y username
        //const username='sss'
        if(username){
                    }
        return (
            <div>
                    <div className="loader"></div>
                <div id="app">
                    <div className="main-wrapper main-wrapper-1">
                        <div className="navbar-bg"></div>
                        <Nav/>
                        <Sidebar/>
                      <div className="main-content">
                    <Switch>
                    {
                         routes.map((route,i)=>{
                             if(route.permission){
                                console.log(route.permission)
                                if(Roles.userIsInRole(Meteor.userId(),route.permission)){
                                    return <SwitchRoutes key={i} {...route} />
                                }
                                else{
                                    //alert('ACCESO DENEGADO')
                                    console.log('ACCESO DENEGADO')
                                }
                             }else{
                                return <SwitchRoutes key={i} {...route} />
                             }
                         })
                    }
                    </Switch>
                      </div>
                        <footer className="main-footer">
                            <div className="footer-left">
                                <a href="templateshub.net">Templateshub</a>
                            </div>
                            <div className="footer-right">
                            </div>
                        </footer>
                    </div>
                </div>

            </div>
        )
    }
}
export default withTracker(params=>{
    return{
        username:Meteor.user()//las variables reactivas {se van a actualizar en tiempo real}
    }
})(Layout)
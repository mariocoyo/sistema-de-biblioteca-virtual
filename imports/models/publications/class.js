import { Class } from 'meteor/jagi:astronomy';


const publicationsCollections = new Mongo.Collection('publications',{idGeneration:'MONGO'});//vamos acrear nuestra coleccion o tablas en mongodb
export const publicationsClass = Class.create({
  name: 'publicationsClass',
  collection:publicationsCollections,
  fields: {
    title:{
        type:String,
        //default:'hola mundo',//valor por defecto
        //optional:true//condicional de que el campo no es obligatorio llenarlo
    },
    description:{
        type:String,
    },
    phone:{
        type:String,
    },
    price:{
        type:Number,
    },
    startDate:{
        type:Date
    },
    endDate:{
        type:Date
    },
    nameCategory:{
        type:String
    },
    
    idfile:{
        type:String
    },
    urlfile:{
        type:String
    },
    active:{
        type:Boolean,
        default:true
    },
    idCategory:{
      type:Mongo.ObjectID
    },
    username:String,
    created_view:{
      type:Date
    },
    typepub:{
      type:String,
      default:'image'
    }
  }
});

if(Meteor.isServer){
    publicationsClass.extend({
      fields:{
        user:String,
      },
      behaviors: {
        timestamp: {
          hasCreatedField: true,
          createdFieldName: 'createdAt',
          hasUpdatedField: true,
          updatedFieldName: 'updatedAt'
        }
      }
    });
}
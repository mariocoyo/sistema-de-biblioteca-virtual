import { publicationsClass } from './class';

export const queryPublications=class{
  constructor(options,mthis){
    this.options=options
    this.mthis=this
  }
  getPublications(){
     try{
      if(Roles.userIsInRole(Meteor.userId(),"allpublicationuser")){
        return publicationsClass.find({user:Meteor.userId()},{sort:{createAt:-1}})
      }else{
        throw new Meteor.Error(403,'ACCESO DENEGADO')
      }
     }catch(error){
       console.log(error)
     }
  }
  getOnePublication(){
    try {
      if(this.options.pub)
           return publicationsClass.find({_id:this.options.pub,user:Meteor.userId()},{sort:{createAt:-1}})
    } catch (error) {
      console.log(error)
    }
  }
  getAllPublications(){
    try {
      console.log(this.options.category)
      if(this.options.category){
        return publicationsClass.find({active:true,idCategory:this.options.category._id},{sort:{createAt:-1}})
      }else{
        return publicationsClass.find({active:true},{sort:{createAt:-1}})
      }
    } catch (error) {
      console.log(error)      
    }
  }
}


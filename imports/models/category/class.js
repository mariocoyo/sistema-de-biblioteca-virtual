import { Class } from 'meteor/jagi:astronomy';

const categoryCollection = new Mongo.Collection('category',{idGeneration:'MONGO'});//vamos acrear nuestra coleccion o tablas en mongodb

export const categoryClass = Class.create({
  name: 'categoryClass',
  collection:categoryCollection,
  fields: {
    name:{
        type:String,
        validators: [{
          type: 'minLength',
          param: 3,
          resolveError({ name, param }) {
            return `El tamaño minimo de categoria tiene que ser ${param}`;
          }
        }]
    },
    description:{
      type:String,
    },
    active:{
      type:Boolean,
      default:true
    }
  }
});
if(Meteor.isServer){
  categoryClass.extend({
    fields:{
      user:String,
    },
    behaviors: {
      timestamp: {
        hasCreatedField: true,
        createdFieldName: 'createdAt',
        hasUpdatedField: true,
        updatedFieldName: 'updatedAt'
      }
    }
  })
}
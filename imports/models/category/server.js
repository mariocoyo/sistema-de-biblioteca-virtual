import {categoryClass} from './class'
import {queryCategory} from './query'

categoryClass.extend({
    meteorMethods:{
        createNewCategory(form){
            //console.log(form)
            this.set(form)
            this.user= Meteor.userId()
            this.save()
            return 'Creado con exito'
        }
    
    }
  })
Meteor.publish('category',function(options,type){
    try {
        const subs=new queryCategory(options,this)
        return subs[type]()
    } catch (error) {
        this.stop()
        throw new Meteor.Error(403,error.reason)
    }
})
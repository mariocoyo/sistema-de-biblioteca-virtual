import { categoryClass } from './class';

export const queryCategory=class{
  constructor(options,mthis){
    this.options=options
    this.mthis=mthis
  }
  getCategory(){
    try{
        return categoryClass.find({active:true},{short:{createAt:-1}})
    }catch(error){
      console.log(error)
    }
  }
  getAllCategory(){
    try{
        return categoryClass.find({},{sort:{createAt:-1}})
    }catch(error){
      console.log(error)
    }
  }
}

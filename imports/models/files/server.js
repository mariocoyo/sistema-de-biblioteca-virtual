import { Meteor } from "meteor/meteor";
import UserFiles from './class'
Meteor.methods({
    RemoveFiles(id){
        UserFiles.remove({_id:id},function(error){
            if(error){
                console.log('no se pudo remover el archivo'+error.reason)
            }
            else{
                console.log('eliminado con exito')
            }

        })
    }
})
//para sacar todos nuestros archivos
Meteor.publish('feles.all',function(){
    return UserFiles.find().cursor
})